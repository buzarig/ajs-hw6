"use strict";

const search = document.getElementById("search");
const result = document.getElementById("result");

async function getIP() {
  const response = await fetch("https://api.ipify.org/?format=json");
  const data = await response.json();
  return data.ip;
}

async function getLocation(ip) {
  const response = await fetch(
    `http://ip-api.com/json/${ip}?fields=status,continent,country,regionName,city,district`
  );
  const data = await response.json();
  return data;
}

async function showLocation() {
  const ip = await getIP();
  const data = await getLocation(ip);
  if (data.status === "success") {
    result.innerHTML = `
        <p>Континент: ${data.continent}</p>
        <p>Країна: ${data.country}</p>
        <p>Регіон: ${data.regionName}</p>
        <p>Місто: ${data.city}</p>
        <p>Район: ${data.district}</p>
      `;
  } else {
    result.innerHTML = `<span>Не вдалося знайти місцезнаходження за IP: ${ip}.</span>`;
  }
}

search.addEventListener("click", showLocation);
